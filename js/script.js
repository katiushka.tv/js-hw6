"use strict"

function createNewUser() {
    let userName = prompt("Enter your name, please.");
    let userLastName = prompt("Enter your last name.");
    let userBirthday = prompt("Now, enter your date of birth.","dd.mm.yyyy");
    while (userBirthday[2] !== "." && userBirthday[5] !== ".") {
        userBirthday = prompt("Enter your date of birth correctly.","dd.mm.yyyy");
    }
    return  {
        firstName: userName,
        lastName: userLastName,
        birthday: userBirthday,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge() {
            let dateParts = userBirthday.split(".");
            let birthDate = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
            let now = new Date();
            let age = now.getFullYear() - birthDate.getFullYear();
            let m = now.getMonth() - birthDate.getMonth();
            if (m <0 || m === 0 && now.getDate() < birthDate.getDate()) {
                age --;
            }
            return age;
        },
        getPassword() {
            return (this.firstName[0]).toUpperCase() + (this.lastName).toLowerCase() + this.birthday.slice(-4);
        }
    }
}
const newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(`Your login: ${newUser.getLogin()}`);
console.log(`Your password: ${newUser.getPassword()}`);